package tutorial;

import sim.util.Bag;
import sim.util.Int2D;
import tutorial.model.Constants;
import tutorial.model.Location;
import tutorial.model.Planete;
import tutorial.model.balise.Balise;
import tutorial.model.environnement.Fruit;
import tutorial.model.environnement.Ressource;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Tools {

    /**
     * Génère un entier compris entre inf et sup inclus
     * @param inf borne inférieure
     * @param sup borne supérieure
     * @return l'entier généré
     */
    public static int random(int inf, int sup) {
        Random r = new Random();
        return r.nextInt(sup + 1 - inf) + inf;
    }

    /**
     * Retourne aléatoirement une case adjacente à la case passée en paramètre
     * @param location case de référence
     * @param p planète
     * @return la case aléatoire adjacente
     */
    public static Int2D getRandomLocationAdjacente(Planete p, Int2D location) {
        int x_offset,y_offset;
        do {
                x_offset = Tools.random(-1, 1);
                y_offset = Tools.random(-1, 1);
        } while(x_offset == 0 && y_offset == 0);

        return new Int2D(p.getYard().stx(location.x + x_offset), p.getYard().sty(location.y + y_offset));
    }

    /**
     * Vérifie si une case de la planète contient au moins une instance d'une classe
     * @param p planète
     * @param x coordonnée x de la case à inspecter
     * @param y coordonnée y de la case à inspecter
     * @param c classe dont on vérifie la présence
     * @return la valeur de vérité
     */
    public static boolean contientClasse(Planete p, int x, int y, Class c) {
        boolean contient = false;
        Bag b = p.yard.getObjectsAtLocation(x,y);
        if(b != null) {
            for (Object o : b) {
                if(c.isInstance(o)) {
                    contient = true;
                }
            }
        }
        return contient;
    }

    /**
     * Renvoie l'instance de la ressource se trouvant à un emplacement donné
     * @param p planète
     * @param x coordonnée x de l'emplacement
     * @param y coordonnée y de l'emplacement
     * @param r classe de ressource recherchée
     * @return l'instance de la ressource présente
     */
    public static Ressource getRessource(Planete p, int x, int y, Class r) {
        Bag b = p.getYard().getObjectsAtLocation(x,y);
        if(b != null) {
            for (Object o : b) {
                if(r.isInstance(o)) {
                    return (Ressource) o;
                }
            }
        }
        return null;
    }


    /**
     * Calcule la distance entre deux cases sans compter
     * les déplacements en diagonale
     * @param p planète
     * @param aX coordonnée X première case
     * @param aY coordonnée Y première case
     * @param bX coordonnée X deuxième case
     * @param bY coordonnée Y deuxième case
     * @return distance entre les deux cases
     */
    public static int distance(Planete p, int aX, int aY, int bX, int bY) {
        int dX = Math.abs(aX - bX);
        if(dX > p.getGridSize()/2) {
            dX = p.getGridSize() - dX;
        }
        int dY = Math.abs(aY - bY);
        if(dY > p.getGridSize()/2) {
            dY = p.getGridSize() - dY;
        }

        return dX + dY;
    }

    /**
     * Calcule l'emplacement le plus proche de celui passé en paramètre.
     * @param p planète
     * @param depart emplacement de départ
     * @param locations liste d'emplacements
     * @return l'emplacement le plus proche
     */
    public static Location plusProche(Planete p, Location depart, List<Location> locations) {
        int distance = distance(p, depart.getX(),depart.getY(),
                locations.get(0).getX(),locations.get(0).getY());
        Location retour = locations.get(0);
        for (Location l : locations) {
            int d = distance(p, depart.getX(),depart.getY(),l.getX(),l.getY());
            if(d < distance) {
                distance = d;
                retour = l;
            }
        }
        return retour;
    }

    /**
     * Calcule l'emplacement de la balise la plus ancienne
     * @param locations liste des balises
     * @param p planète
     * @return
     */
    public static Location plusAncienne(final List<Location> locations, final Planete p) {
        Location toReturn = locations.get(0);
        int intensiteMin = Balise.getIntensiteMax(p) + 1;

        for (Location l : locations) {
            Bag b = p.yard.getObjectsAtLocation(l.getX(),l.getY());
            for(Object o : b) {
                if (o instanceof Balise) {
                    if (((Balise) o).getIntensite() < intensiteMin) {
                        intensiteMin = ((Balise) o).getIntensite();
                        toReturn = l;
                    }
                }
            }
        }
        return toReturn;
    }

    public static void filtrerPlantes(Planete p, ArrayList<Location> plantes, int etat) {
        plantes.removeIf(loc -> ! (((Fruit)Tools.getRessource(p,loc.getX(),loc.getY(),Fruit.class)).getEtat() == etat));
    }

}
