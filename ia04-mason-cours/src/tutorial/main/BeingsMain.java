package tutorial.main;

import sim.display.Console;
import tutorial.gui.PlaneteWithUI;
import tutorial.model.Planete;
import tutorial.model.PlaneteManager;

public class BeingsMain {
	
	public static void main(String[] args) {
        runUI();
	}
	public static void runUI() {

		Planete model = new Planete(System.currentTimeMillis());
		PlaneteWithUI gui = new PlaneteWithUI(model);
		Console console = new Console(gui);

		console.setVisible(true);
	}
}
