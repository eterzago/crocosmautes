package tutorial.model;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import tutorial.model.crocosmaute.Crocosmaute;

import java.util.ArrayList;

public class Vaisseau implements Steppable {
    public Stoppable stoppable;
    private int x, y;
    private int oxygene;
    private int fruits;
    private int minerai;
    private ArrayList<Crocosmaute> crocosmautes;

    public Vaisseau() {
        this.oxygene = Constants.STOCK_OXYGENE;
        this.fruits = Constants.STOCK_FRUITS;
        this.minerai = 0;
        this.crocosmautes = new ArrayList<Crocosmaute>();
    }

    @Override
    public void step(SimState simState) {
        Planete p = (Planete) simState;
        if (minerai >= Constants.MINERAI_NECESSAIRE) {
            this.signal();
        }
    }

    /**
     * Traite des unités de minerai et incrémente le stock d'oxygène
     *
     * @param nbMinerai nombre d'unités de minerai à traiter
     */
    public void traiterMinerai(int nbMinerai) {
        this.minerai += nbMinerai;
        this.oxygene += nbMinerai * Constants.RATIO_MINERAI_OXYGENE;
    }

    /**
     * Envoie un signal à tous les crocosmautes afin qu'ils reviennent au vaisseau
     */
    public void signal() {
        int i = 0;
        for (Crocosmaute c : this.crocosmautes) {
            c.signal();
            i++;
        }
        System.out.println("Signaux : " + i);
    }

    public void inscrireCrocosmaute(Crocosmaute c) {
//        System.out.println("Croco inscrit");
        this.crocosmautes.add(c);
    }

    public void oublierCrocosmaute(Crocosmaute c) {
        this.crocosmautes.remove(c);
    }

    public void viderCrocosmautes() {
        this.crocosmautes.clear();
    }

    private int compterCrocosmautes() {
        return this.crocosmautes.size();
    }


    public boolean estPretPourLeDepart(Planete p) {
        try {
//            System.out.println("Compter "+this.compterCrocosmautes());
//            System.out.println(p.yard.getObjectsAtLocation(this.getX(), this.getY()).size()-1);

            int count = 0;
            for (Object o : p.yard.getObjectsAtLocation(this.getX(), this.getY())) {
                if (o instanceof Crocosmaute) {
                    count++;
                }
            }

            if (this.compterCrocosmautes() == count) {

//                for (Object o :p.getYard().getObjectsAtLocation(this.getX(), this.getY())){
//                    if (o instanceof Crocosmaute){
//                        ((Crocosmaute) o).mourir(p);
//                    }
//                }

                return true;
            }
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public int countCrocosmautes() {
        return this.crocosmautes.size();
    }


    public void viderMinerai() {
        this.minerai = 0;
    }

    public void decrementerFruits(int nbFruits) {
        this.fruits -= nbFruits;
    }

    public void incrementerFruits(int nbFruits) {
        this.fruits += nbFruits;
    }

    public void decrementerOxygene(int nbOxygene) {
        this.oxygene -= nbOxygene;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "[" + this.x + "," + this.y + "]"
                + "o=" + this.oxygene
                + " m=" + this.minerai
                + " f=" + this.fruits;
    }


    public void setStoppable(Stoppable stoppable) {
        this.stoppable = stoppable;
    }

    public int getOxygene() {
        return oxygene;
    }

    public int getFruits() {
        return fruits;
    }

    public int getMinerai() {
        return minerai;
    }

    public ArrayList<Crocosmaute> getCrocosmautes() {
        return crocosmautes;
    }

    public void setCrocosmautes(ArrayList<Crocosmaute> crocosmautes) {
        this.crocosmautes = crocosmautes;
    }
}
