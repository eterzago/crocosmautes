package tutorial.model;

import sim.engine.Steppable;
import sim.engine.Stoppable;
import tutorial.Tools;
import tutorial.model.alien.Alien;
import tutorial.model.alien.AlienHerbivore;
import tutorial.model.crocosmaute.Crocosmaute;

import java.util.ArrayList;

public abstract class Creature implements Steppable {
    public int x,y;
    public Stoppable stoppable;
    protected int distanceDeplacement,distancePerception;

    /* Effectue un déplacement d'une seule case en direction
            * de la case donnée
     * @param p planète
     * @param x coordonnée X de la case à atteindre
     * @param y coordonnée Y de la case à atteindre
     */
    protected void avancerDeUnPasVers(Planete p, int x, int y) {


        if (this.x!=x){
            int dist1 = Tools.distance(p, this.x+1, this.y, x, y);
            int dist2 = Tools.distance(p, this.x-1, this.y, x, y);

            if (dist1<dist2){
                this.x = p.getYard().stx(this.x+1);
            } else {
                this.x = p.getYard().stx(this.x-1);
            }
        } else {
            int dist1 = Tools.distance(p, this.x,this.y+1, x,  y);
            int dist2 = Tools.distance(p, this.x, this.y-1,x,  y);
            if (dist1<dist2){
                this.y = p.getYard().sty(this.y+1);
            } else {
                this.y = p.getYard().sty(this.y-1);
            }
        }
        p.getYard().setObjectLocation(this,this.x, this.y);
    }

    /**
     * Effectue un déplacement d'autant de cases que
     * possible vers la case donnée
     * @param p planète
     * @param x coordonnée X de la case à atteindre
     * @param y coordonnée Y de la case à atteindre
     */
    protected void avancerVers(Planete p, int x, int y) {
        if(this instanceof Crocosmaute) {
            ((Crocosmaute)this).decrementerFaim();
        }
        /* Si on peut atteindre la case en un déplacement */
        if(Tools.distance(p, x, y, this.x, this.y) <= this.getDistanceDeplacement())
        {
            p.yard.setObjectLocation(this, x, y);
            this.x = x;
            this.y = y;
        }
        else
        {
            int nbPas;
            //int distX = Math.abs(this.x - x);
            //int distY = Math.abs(this.y - y);

            int distX = Math.abs(this.x - x);
            if(distX > p.getGridSize()/2) {
                distX = p.getGridSize() - distX;
            }
            int distY = Math.abs(this.y - y);
            if(distY > p.getGridSize()/2) {
                distY = p.getGridSize() - distY;
            }

            if(distX > distY)
            {
                /* Nombre de pas à effectuer horizontalement */
                nbPas = this.getDistanceDeplacement() - (this.getDistanceDeplacement() - distX);

                int distDirecte = Tools.distance(p, this.x+nbPas, this.y, x, y);
                int distThor = Tools.distance(p, this.x-nbPas, this.y, x, y);

                if (distDirecte < distThor){
                    p.yard.setObjectLocation(this, this.x + nbPas, this.y);
                    this.x += nbPas;
                } else {
                    p.yard.setObjectLocation(this, this.x - nbPas, this.y);
                    this.x -= nbPas;
                }
            }
            else {
                /* Nombre de pas à effectuer verticalement */
                nbPas = this.getDistanceDeplacement() - (this.getDistanceDeplacement() - distY);

                int distDirecte = Tools.distance(p, this.x, this.y+nbPas, x, y);
                int distThor = Tools.distance(p, this.x, this.y-nbPas, x, y);

                if (distDirecte < distThor){
                    p.yard.setObjectLocation(this, this.x, this.y + nbPas);
                    this.y += nbPas;
                } else {
                    p.yard.setObjectLocation(this, this.x, this.y - nbPas);
                    this.y -= nbPas;
                }
            }
        }
    }


    /**
     * Se déplace dans une direction aléatoire
     * @param planete
     * @return vrai si tout s'est bien passé
     */
    protected void moveInRandomDirection(Planete planete) {
        if(this instanceof Crocosmaute) {
            ((Crocosmaute)this).decrementerFaim();
        }
        boolean done = false;
        int n = Tools.random(0, Planete.NB_DIRECTIONS-1);
        planete.yard.remove(this);

        switch(n) {

            case 0:
                planete.yard.setObjectLocation(this, planete.yard.stx(x-this.distanceDeplacement), y);
                this.x = planete.yard.stx(x-this.distanceDeplacement); // Updating coordinates
                done = true;
                break;
            case 1:
                planete.yard.setObjectLocation(this, planete.yard.stx(x+this.distanceDeplacement), y);
                this.x = planete.yard.stx(x+this.distanceDeplacement);
                done = true;
                break;
            case 2:
                planete.yard.setObjectLocation(this,x, planete.yard.sty(y-this.distanceDeplacement));
                this.y = planete.yard.sty(y-this.distanceDeplacement);
                done = true;
                break;
            case 3:
                planete.yard.setObjectLocation(this, x, planete.yard.sty(y+this.distanceDeplacement));
                this.y = planete.yard.sty(y+this.distanceDeplacement);
                done = true;
                break;
            case 4:
                planete.yard.setObjectLocation(this, planete.yard.stx(x-this.distanceDeplacement), planete.yard.sty(y-this.distanceDeplacement));
                this.x = planete.yard.stx(x-this.distanceDeplacement);
                this.y = planete.yard.sty(y-this.distanceDeplacement);
                done = true;
                break;
            case 5:
                planete.yard.setObjectLocation(this, planete.yard.stx(x+1), planete.yard.sty(y-this.distanceDeplacement));
                this.x = planete.yard.stx(x+this.distanceDeplacement);
                this.y = planete.yard.sty(y-this.distanceDeplacement);
                done = true;
                break;
            case 6:
                planete.yard.setObjectLocation(this, planete.yard.stx(x+1), planete.yard.sty(y+this.distanceDeplacement));
                this.x = planete.yard.stx(x+this.distanceDeplacement);
                this.y = planete.yard.sty(y+this.distanceDeplacement);
                done = true;
                break;
            case 7:
                planete.yard.setObjectLocation(this, planete.yard.stx(x-this.distanceDeplacement), planete.yard.sty(y+this.distanceDeplacement));
                this.x = planete.yard.stx(x-1);
                this.y = planete.yard.sty(y+1);
                done = true;
                break;
        }
    }

    /**
     * Renvoie une liste des emplacements perceptibles par le
     * crocosmaute qui contiennent un certain type de ressource
     * @param p planète
     * @param ressource le type de ressource recherché
     * @return la liste des emplacements contenant la ressource
     */
    protected ArrayList<Location> ressourceEnVue (Planete p, Class ressource) {
        /*if (ressource.isAssignableFrom(Ressource.class)){
            throw new IllegalArgumentException("Ce n'est pas une ressource !");
        }*/
        ArrayList<Location> retour = new ArrayList<>();

        /* Construction d'un rayon de perception */
        int dist = this.getDistancePerception();
        int iMin = p.yard.stx(this.x - dist);
        int iMax = p.yard.stx(this.x + dist);
        int jMin = p.yard.sty(this.y - dist);
        int jMax = p.yard.sty(this.y + dist);

        for(int i = iMin; i < iMax; i += 1)
        {
            for(int j = jMin; j < jMax; j += 1)
            {
                if(Tools.contientClasse(p,i%p.yard.getWidth(),j%p.yard.getHeight(), ressource))
                {
                    retour.add(new Location(i%p.yard.getWidth(),j%p.yard.getHeight()));
                }
            }
        }
        return retour;
    }

    int getDistanceDeplacement() {
        return distanceDeplacement;
    }
    int getDistancePerception() {
        return distancePerception;
    }
}
