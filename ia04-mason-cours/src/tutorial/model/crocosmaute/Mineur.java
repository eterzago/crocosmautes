package tutorial.model.crocosmaute;

import sim.util.Bag;
import tutorial.Tools;
import tutorial.model.Constants;
import tutorial.model.Location;
import tutorial.model.alien.Alien;
import tutorial.model.balise.Balise;
import tutorial.model.balise.TypeBalise;
import tutorial.model.environnement.Minerai;
import tutorial.model.Planete;
import java.util.ArrayList;
import java.util.Iterator;

public class Mineur extends Crocosmaute {
    private int chargeMinerai;

    public Mineur(int distanceDeplacement, int distancePerception) {
        super(distanceDeplacement, distancePerception);
        this.chargeMinerai = 0;
    }

    @Override
    protected void strategie(Planete p) {
        /* Gérer le danger le plus immédiat */
        if(Tools.contientClasse(p,this.x,this.y,Alien.class))
        {
            this.attaquer(p);
        }
        /* Si l'on est au vaisseau, y déposer le minerai */
        if(this.x == p.getPosVaisseau().getX() && this.y == p.getPosVaisseau().getY()
                && this.getChargeMinerai() > 0)
        {
            this.reparerVaisseau(p);
        }
        /* Si l'on ne peut porter plus de minerai, retour au vaisseau */
        else if(this.getChargeMinerai() == Constants.CHARGE_MINERAI_MAX)
        {
            this.retourVaisseau(p);
        }
        /* Si l'on se trouve sur du minerai, le miner */
        else if(Tools.contientClasse(p,x,y,Minerai.class))
        {
            this.miner(p);
        }
        else if( ! gestionAlien(p))
        {
            ArrayList<Location> mineraiEnVue = this.ressourceEnVue(p,Minerai.class);
            ArrayList<Location> balisesEnVue = this.ressourceEnVue(p,Balise.class);
            Iterator <Location> i = balisesEnVue.iterator();
            while(i.hasNext()) {
                Location l = i.next();
                boolean estMinerai = false;

                Bag b = p.yard.getObjectsAtLocation(l.getX(),l.getY());
                if(b != null) {
                    for (Object o : b) {
                        if(o instanceof Balise) {
                            if(((Balise)o).getType() == TypeBalise.MINERAI) {
                                estMinerai = true;
                            }
                        }
                    }
                }
                if(!estMinerai) {
                    i.remove();
                }
            }

            /* Si du minerai est à proximité, s'y rendre */
            if(!mineraiEnVue.isEmpty())
            {
                Location loc = Tools.plusProche(p, new Location(this.x,this.y), mineraiEnVue);
                this.avancerVers(p, loc.getX(), loc.getY());
            }
            /* Si une balise minerai est à proximité, s'y rendre */
            else if(!balisesEnVue.isEmpty())
            {
                // On va vers la balise à la plus faible trace
                Location there = Tools.plusAncienne(balisesEnVue, p);
                this.avancerVers(p, there.getX(), there.getY());
            }
            else
            {
                moveInRandomDirection(p);
            }
        }

    }

    /**
     * Mine une unité de minerai, réduit la dureté de celle-ci de 1.
     * @param planete
     */
    private void miner(Planete planete) {
        Bag b = planete.yard.getObjectsAtLocation(this.x,this.y);
        if(b != null) {
            for (Object o: b)
            {
                if(o instanceof Minerai)
                {
                    ((Minerai) o).decrementerQuantite(1);
                    /* Le crocosmaute a fini de miner */
                    if(((Minerai) o).getQuantite() == 0) {
                        this.chargeMinerai += 1;
                    }
                }
            }
        }
    }

    /**
     * Dépose le minerai porté au vaisseau.
     * @param p planète
     */
    private void reparerVaisseau(Planete p) {
        if(this.x == p.getVaisseau().getX() && this.y == p.getVaisseau().getY()) {
            p.getVaisseau().traiterMinerai(this.getChargeMinerai());
            this.setChargeMinerai(0);
        }
    }

    public int getChargeMinerai() {
        return chargeMinerai;
    }

    private void setChargeMinerai(int chargeMinerai) {
        this.chargeMinerai = chargeMinerai;
    }

    @Override
    public String toString() {
        return super.toString() + " m=" + this.getChargeMinerai();
    }
}