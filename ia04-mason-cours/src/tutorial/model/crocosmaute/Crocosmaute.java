package tutorial.model.crocosmaute;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.util.Bag;
import tutorial.Tools;
import tutorial.model.Constants;
import tutorial.model.Location;
import tutorial.model.Planete;
import tutorial.model.Creature;
import tutorial.model.alien.Alien;
import tutorial.model.alien.AlienCarnivore;
import tutorial.model.alien.AlienHerbivore;
import tutorial.model.environnement.Fruit;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class Crocosmaute extends Creature {
    private int oxygene;
    private int faim;
    private int chargeFruits;

    private boolean signal;


    Crocosmaute(int distanceDeplacement, int distancePerception)
    {
        this.faim = Constants.SATIETE;
        this.oxygene = Constants.OXYGENE_MAX;
        this.chargeFruits = 0;
        this.signal = false;

        this.distanceDeplacement = distanceDeplacement;
        this.distancePerception = distancePerception;
                 // Ainsi, il ne peut pas se déplacer à l'aveuglette
    }

    @Override
    public void step(SimState simState) {
        Planete p = (Planete)simState;
        /* Si l'on n'a pas effectué d'action pour assurer sa survie */
        if (!this.survie(p))
        {
            /* Lancer la stratégie spécifique au métier */
            this.strategie(p);
        }
    }

    /**
     * Détaille la stratégie de chaque crocosmaute
     * en fonction de son métier.
     * @param p planète
     */
    protected abstract void strategie(Planete p);

    /**
     * Stratégie de base de tout crocosmaute, qui assure seulement
     * sa survie et son retour au vaisseau s'il reçoit un signal
     * @param p planète
     * @return vrai si le crocosmaute a effectué une action
     */
    private boolean survie(Planete p) {
        boolean action = false;
//        System.out.println(this.getClass().getSimpleName()+Integer.toHexString(this.hashCode())+" survie : o="+this.getOxygene()+",f="+this.getFaim());

        /* Si une jauge tombe à zéro, mort */
        if(this.faim <= 0 || this.oxygene <= 0)
        {
//            System.out.println(this.getClass().getSimpleName()+Integer.toHexString(this.hashCode()) + " DYING o="+this.getOxygene()+",f="+this.getFaim());
            action = true;
            this.mourir(p);
        }
        /* Si une des deux jauges est critique */
        else if(this.seuilFaimCritique(p)
                || this.seuilOxygeneCritique(p))
        {
            /* Si l'on a plus faim que l'on ne manque d'oxygène */
            if(this.getFaim() <= this.getOxygene() && this.getFaim() <= Constants.SEUIL_CRITIQUE_SATIETE) // FIXME pas très accurate
            {
//                System.out.println(this.getClass().getSimpleName()+Integer.toHexString(this.hashCode()) + " FAIM");
                /* Si l'on porte des fruits, en manger */
                if(this.getChargeFruits() > 0)
                {
                    action = true;
//                    System.out.println(this.getClass().getSimpleName()+Integer.toHexString(this.hashCode()) + " EATING");
                    this.manger();
                }
                /* Si on est au vaisseau, prendre un fruit */ //TODO Un n'est parfois pas assez
                else if (this.x == p.getPosVaisseau().getX()
                        && this.y == p.getPosVaisseau().getY()
                        && p.getVaisseau().getFruits() > 0)
                {
                    action = true;
//                    System.out.println(this.getClass().getSimpleName()+Integer.toHexString(this.hashCode()) + " PREND FRUIT");
                    this.prendreFruit(p);
                }
                /* Si on est sur un arbre qui porte des fruits, en cueillir un */
                else if (Tools.contientClasse(p,this.x,this.y,Fruit.class)
                        && ((Fruit)(Tools.getRessource(p,this.x,this.y,Fruit.class))).getEtat() == Constants.ETAT_ARBRE)
                {
                    action = true;
                    System.out.println("cueillir");
                    this.cueillirUnFruit(p);
                }
                else
                {
                    /* Liste des emplacements contenant des arbres fruitiers */
                    ArrayList<Location> arbresEnVue = this.ressourceEnVue(p,Fruit.class);
                    Iterator <Location> i = arbresEnVue.iterator();
                    while(i.hasNext()) {
                        Location l = i.next();
                        Bag b = p.getYard().getObjectsAtLocation(l.getX(),l.getY());
                        boolean estArbre = false;
                        Iterator j = b.iterator();
                        while(j.hasNext()) {
                            Object o = j.next();
                            if(o instanceof Fruit) {
                                if(((Fruit)o).getEtat() == Constants.ETAT_ARBRE) {
                                    estArbre = true;
                                }
                            }
                        }
                        if(!estArbre) {
                            i.remove();
                        }
                    }
                    /* Si on se trouve à proximité d'un arbre fruitier, s'en approcher */
                    if(!arbresEnVue.isEmpty())
                    {
                        action = true;
                        System.out.println("arbre fruitier");
                        Location newLoc = Tools.plusProche(p, new Location(this.x,this.y),arbresEnVue);
                        this.avancerVers(p,newLoc.getX(),newLoc.getY());
                    }
                    /* Sinon, avancer vers la fusée */
                    else
                    {
                        action = true;
//                        System.out.println(this.getClass().getSimpleName()+Integer.toHexString(this.hashCode())+" go vaisseau");
                        this.retourVaisseau(p);
                    }
                }
            }
            /* Si l'on suffoque plus que l'on n'a faim */
            else if(this.getFaim() > this.getOxygene() && this.getOxygene() <= Constants.SEUIL_CRITIQUE_OXYGENE)
            {
                /* Si l'on est au vaisseau, se recharger en oxygène */
                if(this.x == p.getPosVaisseau().getX()
                        && this.y == p.getPosVaisseau().getY()
                        && p.getVaisseau().getOxygene() > 0)
                {
                    action = true;
                    this.rechargerOxygene(p);
                }
                /* Sinon, avancer vers la fusée */
                else
                {
                    action = true;
                    retourVaisseau(p);
                }
            }
        }
        /* Si l'on est au vaisseau et que l'on peut prendre des fruits,
        en prendre au maximum
         */
        else if(this.getChargeFruits() <= 0
                && this.x == p.getPosVaisseau().getX()
                && this.y == p.getPosVaisseau().getY()
                && p.getVaisseau().getFruits() > 0)
        {
            while(this.getChargeFruits() < Constants.CHARGE_FRUITS_MAX-1
                    && p.getVaisseau().getFruits() > 0)
            {
                this.prendreFruit(p);
            }
            action = true;
        }
        /* Si notre charge de fruits est critique */
        else if(this.getChargeFruits() <= 0
                && p.getVaisseau().getFruits() > 0)
        {
            action = true;
            retourVaisseau(p);
        }
        /* Si le vaisseau a envoyé le signal de retour, s'y rendre */
        else if(this.isSignal())
        {
            if(!(this.x == p.getVaisseau().getX() && this.y == p.getVaisseau().getY()))
            {
                System.out.println("SIGNAL");
            }

            action = true;
            retourVaisseau(p);
        }
        //if(!action) {
        //    System.out.println("NO SURVIVAL NEEDED");
        //}
        return action;
    }

    /**
     * Vérifie si le niveau d'oxygène lui permet tout juste de revenir au vaisseau
     * @return valeur de vérité
     */
    private boolean seuilOxygeneCritique(Planete p) {
        int distanceVaisseau = Tools.distance(
                p, this.x, this.y, p.getPosVaisseau().getX(), p.getPosVaisseau().getY()
        );

        return (this.getOxygene() <= distanceVaisseau + Constants.MARGE_OXYGENE
                || this.getOxygene() <= Constants.SEUIL_CRITIQUE_OXYGENE);
    }

    /**
     * Vérifie si le niveau de satiété lui permet tout juste de revenir au vaisseau
     * @return valeur de vérité
     */
    private boolean seuilFaimCritique(Planete p) {
        int distanceVaisseau = Tools.distance(
                p, this.x, this.y, p.getPosVaisseau().getX(), p.getPosVaisseau().getY()
        );
        return (this.getFaim() <= distanceVaisseau + Constants.MARGE_SATIETE
                || this.getFaim() <= Constants.SEUIL_CRITIQUE_SATIETE);
    }


    /**
     * Permet de se charger en fruit à partir du vaisseau
     * @param p planète
     */
    void prendreFruit(Planete p) {
        p.getVaisseau().decrementerFruits(1);
        this.chargeFruits += 1;
    }

    /**
     *  Recharge la bouteille d'oxygène à partir du vaisseau
     * @param p planète
     */
    private void rechargerOxygene(Planete p) {
        p.getVaisseau().decrementerOxygene(1);
        this.oxygene = Constants.OXYGENE_MAX;
    }

    /**
     *  Mange un fruit et incrémente la jauge de faim
     */
    private void manger() {
        System.out.println(this.getClass().getSimpleName()+" MANGE");
        this.chargeFruits -= 1;
        this.faim += Constants.NUTRITION_FRUIT;
        if(this.faim > Constants.SATIETE) {
            this.faim = Constants.SATIETE;
        }
    }

    /**
     * Supprime le crocosmaute, prie pour son âme et l'enlève de la planète
     * @param p planète
     */
    private void mourir(Planete p) {
        p.yard.remove(this);
        p.getVaisseau().oublierCrocosmaute(this);
        stoppable.stop();
    }

    /**
     * Cueillit un seul fruit
     * @param p planète
     */
    void cueillirUnFruit(Planete p) {
        Bag b = p.yard.getObjectsAtLocation(this.x,this.y);
        for (Object o : b) {
            if(o instanceof Fruit) {
                ((Fruit)o).cueillirFruit();
                this.setChargeFruits(this.getChargeFruits()+1);
            }
        }
    }

    /**
     * Active le voyant signal sur la crocosmontre des crocosmautes,
     * ce qui les fait revenir à la fusée
     */
    public void signal() {
        this.signal = true;
    }

    public void deactivateSignal() {
        this.signal = false;
    }

    /**
     * Avance d'autant de cases que possible vers le vaisseau
     * @param p planète
     */
    void retourVaisseau(Planete p) {
        Location vaisseau = p.getPosVaisseau();
        this.avancerVers(p, vaisseau.getX(), vaisseau.getY());
    }

    void attaquer(Planete p) {
        Bag b = p.getYard().getObjectsAtLocation(this.x,this.y);
        if(b != null) {
            for (Object o : b) {
                if (o instanceof Alien) {
                    ((Alien)o).frapperAlien(Constants.ATTAQUE_CROCO);
                }
            }
        }
    }

    boolean gestionAlien (Planete p)  {
        ArrayList<Location> alienHerbivores = this.ressourceEnVue(p, AlienHerbivore.class);
        ArrayList<Location> alienCarnivores = this.ressourceEnVue(p, AlienCarnivore.class);
        boolean alienTraite = false;
        /* Danger a proximité */
        if (! alienCarnivores.isEmpty()) {
            alienTraite = true;
            ArrayList<Location> potoCrocos = this.ressourceEnVue(p, Crocosmaute.class);
            Location carnivoreProche  = Tools.plusProche(p,new Location(this.x,this.y),alienCarnivores);
            /* on enclenche le combat si on ne prend pas trop risque */
            if(getOxygene() >= 2*Constants.ALIEN_MAX_ATTAQUE + Constants.SEUIL_CRITIQUE_OXYGENE && ! potoCrocos.isEmpty()){
                avancerVers(p,carnivoreProche.getX(),carnivoreProche.getY());
            }
            /* Trop dangereux d'affronter l'alien*/
            else  {
                retourVaisseau(p);
            }
            /* On cherche à éliminer l'alien herbivore avant qu'il devienne une menace */
        } else if (! alienHerbivores.isEmpty()) {
            alienTraite = true;
            Location herbivoreProche  = Tools.plusProche(p,new Location(this.x,this.y),alienHerbivores);
            avancerVers(p,herbivoreProche.getX(),herbivoreProche.getY());
        }
        return alienTraite;
    }

    /**
     * Incrémente la charge de fruits
     * @param nb nombre de fruits à ajouter
     */
    void incrementerChargeFruits(int nb) {
        this.setChargeFruits(this.getChargeFruits() + nb);
    }

    /**
     * Décrémente la charge de fruits
     * @param nb nombre de fruits à retirer
     */
    void decrementerChargeFruits(int nb) {
        this.setChargeFruits(this.getChargeFruits() - nb);
    }

    int getOxygene() {
        return oxygene;
    }

    public void setOxygene(int oxygene) {
        this.oxygene = oxygene;
    }

    int getFaim() {
        return faim;
    }

    public void setFaim(int faim) {
        this.faim = faim;
    }

    public int getChargeFruits() {
        return chargeFruits;
    }

    void setChargeFruits(int chargeFruits) {
        this.chargeFruits = chargeFruits;
    }

    int getDistanceDeplacement() {
        return distanceDeplacement;
    }

    public void setDistanceDeplacement(int distanceDeplacement) {
        this.distanceDeplacement = distanceDeplacement;
    }

    int getDistancePerception() {
        return distancePerception;
    }

    public void setDistancePerception(int distancePerception) {
        this.distancePerception = distancePerception;
    }

    private boolean isSignal() {
        return signal;
    }

    public void decrementerFaim() {--faim;}
    public void decrementerOxygene() {--oxygene;}

    public void frapperCroco (int o2Enleve) {
        this.oxygene -= o2Enleve;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                + "["+this.x + "," + this.y+"]"
                + " o" + this.getOxygene() + " f" + this.getFaim()
                + " perc=" + this.getDistancePerception();
    }

}
