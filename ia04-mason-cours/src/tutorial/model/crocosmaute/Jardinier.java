package tutorial.model.crocosmaute;

import sim.engine.Stoppable;
import sim.util.Bag;
import tutorial.Tools;
import tutorial.model.Constants;
import tutorial.model.Location;
import tutorial.model.Planete;
import tutorial.model.alien.Alien;
import tutorial.model.alien.AlienCarnivore;
import tutorial.model.alien.AlienHerbivore;
import tutorial.model.balise.Balise;
import tutorial.model.balise.TypeBalise;
import tutorial.model.environnement.Eau;
import tutorial.model.environnement.Fruit;
import tutorial.model.environnement.RessourceFactory;

import java.util.ArrayList;
import java.util.Iterator;

public class Jardinier extends Crocosmaute {
    private int chargeEau;

    public Jardinier(int distanceDeplacement, int distancePerception) {
        super(distanceDeplacement, distancePerception);
        this.chargeEau = 0;
    }


    @Override
    protected void strategie(Planete p) {
        /* Gérer en premier les aliens */
        if(Tools.contientClasse(p,this.x,this.y,Alien.class))
        {
            this.attaquer(p);
        }
        /* Si on est au vaisseau, qu'on n'a pas de fruit et qu'il y
         * en a au vaisseau, en prendre */
        else if(this.x == p.getVaisseau().getX()
            && this.y == p.getVaisseau().getY()
            && this.getChargeFruits() == 0 && p.getVaisseau().getFruits() > 0)
        {
            //System.out.println("[Gardener] Picking up fruit @ ship");
            this.prendreFruit(p);
            this.prendreFruit(p);
        }
        /* Si on est au vaisseau et qu'on porte la quantité max de fruit,
           en garder deux et déposer le reste au vaisseau
         */
        else if(this.x == p.getVaisseau().getX()
                && this.y == p.getVaisseau().getY()
                && this.getChargeFruits() == Constants.CHARGE_FRUITS_MAX)
        {
            //System.out.println("[Gardener] Depositing fruit @ ship");
            this.deposerFruits(p,this.getChargeFruits() - 2);
        }
        /* Si ni le vaisseau ni notre sac à dos ne contient de fruits, random */
        else if (this.x == p.getVaisseau().getX()
                && this.y == p.getVaisseau().getY()
                && this.getChargeFruits() <= 0 && p.getVaisseau().getFruits() <= 0) {
            this.moveInRandomDirection(p);
        }
        /* Si on porte la quantité max de fruits, retour au vaisseau */
        else if(this.getChargeFruits() == Constants.CHARGE_FRUITS_MAX)
        {
            //System.out.println("[Gardener] Fruits full, returning to ship");

            this.retourVaisseau(p);
        }
        /*
         * Si on se trouve sur un arbre fruitier, récolter autant
         * de fruits que l'on peut porter
         */
        else if (Tools.contientClasse(p,this.x,this.y,Fruit.class)
                && ((Fruit)(Tools.getRessource(p,this.x,this.y,Fruit.class))).getEtat() == Constants.ETAT_ARBRE)
        {
            //System.out.println("[Gardener] Harvesting tree");

            while(Tools.getRessource(p,this.x,this.y,Fruit.class).getQuantite() > 0
                    && this.getChargeFruits() < Constants.CHARGE_FRUITS_MAX)
            {
                this.cueillirUnFruit(p);
            }
        }
        /* Si l'arrosoir est vide */
        else if(this.getChargeEau() == 0)
        {
            //System.out.println("[Gardener] No more water !");

            Eau eau = (Eau) Tools.getRessource(p,this.x,this.y,Eau.class);
            /* Si l'on se trouve sur une zone d'eau, remplir l'arrosoir */
            if(eau != null)
            {
                //System.out.println("[Gardener] Filling cup @ water");

                while(eau.getQuantite() > 0 && this.getChargeEau() < Constants.CHARGE_EAU_MAX)
                {
                    this.remplirArrosoir(1);
                    eau.decrementerQuantite(1);
                }
            }
            /*Sinon on suit les éventuelles balises pour trouver de l'eau*/
            else
            {
                ArrayList<Location> eauEnVue = this.ressourceEnVue(p,Eau.class);
                ArrayList<Location> balisesEnVue = this.ressourceEnVue(p,Balise.class);
                Iterator<Location> i = balisesEnVue.iterator();

                while(i.hasNext()) {
                    Location l = i.next();
                    boolean estEau = false;

                    Bag b = p.yard.getObjectsAtLocation(l.getX(),l.getY());
                    for (Object o : b) {
                        if(o instanceof Balise) {
                            if(((Balise)o).getType() == TypeBalise.EAU) {
                                estEau = true;
                            }
                        }
                    }
                    if(!estEau) {
                        i.remove();
                    }
                }


                /* Si de l'eau est à proximité, s'y rendre */
                if(!eauEnVue.isEmpty())
                {
                    //System.out.println("[Gardener] Water in sight !");

                    Location here = new Location(this.x,this.y);
                    Location there = Tools.plusProche(p,here,eauEnVue);
                    this.avancerVers(p,there.getX(),there.getY());
                }
                /* Si une balise eau est à proximité, s'y rendre*/
                else if(!balisesEnVue.isEmpty())
                {
                    //System.out.println("[Gardener] Water flag in sight !");
                    //On se dirige vers la balise à la plus faible trace
                    Location there = Tools.plusAncienne(balisesEnVue, p);
                    this.avancerVers(p, there.getX(),there.getY());
                }
                else
                {
                    //System.out.println("[Gardener] Yolo");

                    this.moveInRandomDirection(p);
                }
            }
        }
        /* Si on est sur un fruit non arrosé, l'arroser */
        else if(Tools.contientClasse(p,this.x,this.y,Fruit.class)
                && !((Fruit) (Tools.getRessource(p, this.x, this.y, Fruit.class))).isEstArrose())
        {
            //System.out.println("[Gardener] Watering plant");

            this.arroser(p);
        }
        else if( ! gestionAlien(p))
        {
            ArrayList<Location> plantes = this.ressourceEnVue(p,Fruit.class);
            ArrayList<Location> arbrisseau = (ArrayList<Location>)plantes.clone();
            Tools.filtrerPlantes(p, arbrisseau,Constants.ETAT_ARBRISSEAU);
            filtrerArrosage(p,arbrisseau);
            if(! arbrisseau.isEmpty()) {
                Location loc = Tools.plusProche(p, new Location(this.x,this.y), arbrisseau);
                this.avancerVers(p, loc.getX(), loc.getY());
            } else {
                ArrayList<Location> pousse = (ArrayList<Location>)plantes.clone();
                Tools.filtrerPlantes(p, pousse,Constants.ETAT_POUSSE);
                filtrerArrosage(p,pousse);
                if(! pousse.isEmpty()) {
                    Location loc = Tools.plusProche(p, new Location(this.x,this.y), pousse);
                    this.avancerVers(p, loc.getX(), loc.getY());
                } else if (! plantes.isEmpty()) {
                    Location loc = Tools.plusProche(p, new Location(this.x,this.y), plantes);
                    this.avancerVers(p, loc.getX(), loc.getY());
                } else {
                    /* Si on porte des fruits */
                    if(this.getChargeFruits() > 0)
                    {
                        Bag b = p.getYard().getObjectsAtLocation(this.x, this.y);
                        boolean emptySpot = true;
                        if(b != null) {
                            for (Object o : b) {
                                if(!(o instanceof Crocosmaute)) {
                                    emptySpot = false;
                                }
                            }
                        }

                        /* Si l'on se trouve sur une case vide, planter un fruit */
                        if(emptySpot)
                        {
                            //System.out.println("[Gardener] Planting fruit");

                            this.planter(p);
                        }
                        else
                        {
                            //System.out.println("[Gardener] Yolo");

                            this.moveInRandomDirection(p);
                        }
                    }
                    /* Si l'on n'a aucun fruit */
                    else
                    {
                        //System.out.println("[Gardener] Nothing more to do, returning to ship");
                        this.retourVaisseau(p);
                    }
                }
            }
        }
    }

    /**
     * Plante un fruit à l'emplacement du crocosmaute
     * @param p planète
     */
    private void planter(Planete p) {
        Fruit fruit = (Fruit)RessourceFactory.createRessource(Fruit.class);
        p.yard.setObjectLocation(fruit,this.x,this.y);
        fruit.setX(this.x) ;
        fruit.setY(this.y) ;
        Stoppable stoppable = p.schedule.scheduleRepeating(fruit);
        fruit.setStoppable(stoppable);

        this.decrementerChargeFruits(1);
    }

    /**
     * Arrose la plante située à l'emplacement du crocosmaute
     * @param p planète
     */
    private void arroser(Planete p) {
        Bag b = p.getYard().getObjectsAtLocation(this.x,this.y);
        for (Object o : b) {
            if(o instanceof Fruit) {
                ((Fruit)o).setEstArrose(true);
                this.viderArrosoir(1);
            }
        }
    }

    /**
     * Dépose une certaine quantité de fruits au vaisseau
     * @param p planète
     * @param nbFruits nombre de fruits à déposer
     */
    private void deposerFruits(Planete p, int nbFruits) {
        p.getVaisseau().incrementerFruits(nbFruits);
        this.decrementerChargeFruits(nbFruits);
    }


    /**
     * Incrémente la charge d'eau d'un certain nombre
     * d'unités
     * @param unites nombre d'unités d'eau à ajouter à l'arrosoir
     */
    private void remplirArrosoir (int unites)
    {
        this.setChargeEau(this.getChargeEau() + unites);
    }

    /**
     * Décrémente la charge d'eau d'un certain nombre
     * d'unités
     * @param unites nombre d'unités d'eau à retirer de l'arrosoir
     */
    private void viderArrosoir(int unites) {
        this.setChargeEau(this.getChargeEau() - unites);
    }

    private int getChargeEau() {
        return chargeEau;
    }

    private void setChargeEau(int chargeEau) {
        this.chargeEau = chargeEau;
    }

    @Override
    public String toString() {
        return super.toString() + "e=" + this.getChargeEau() + " f=" + this.getChargeFruits();
    }

    private void filtrerArrosage (Planete p, ArrayList<Location> plantes){
            plantes.removeIf(loc -> (((Fruit)Tools.getRessource(p,loc.getX(),loc.getY(),Fruit.class)).isEstArrose()));
    }
}
