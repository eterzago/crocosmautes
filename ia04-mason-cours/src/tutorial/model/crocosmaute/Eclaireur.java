package tutorial.model.crocosmaute;

import sim.engine.Stoppable;
import sim.util.Bag;
import tutorial.Tools;
import tutorial.model.Constants;
import tutorial.model.Location;
import tutorial.model.alien.Alien;
import tutorial.model.alien.AlienCarnivore;
import tutorial.model.alien.AlienHerbivore;
import tutorial.model.balise.Balise;
import tutorial.model.Planete;
import tutorial.model.balise.TypeBalise;
import tutorial.model.environnement.Eau;
import tutorial.model.environnement.Minerai;

import java.util.ArrayList;

public class Eclaireur extends Crocosmaute {
    private TypeBalise balise;
    private boolean retourFusee;

    public Eclaireur(int distanceDeplacement, int distancePerception) {
        super(distanceDeplacement, distancePerception);
        retourFusee = false;
        balise = null;
    }

    @Override
    protected void strategie(Planete p) {
        Bag b = p.getYard().getObjectsAtLocation(this.x,this.y);
        boolean alien = false;
        boolean eau = false;
        boolean minerai = false;
        if(b != null)
        {
            for (Object o : b) {
                if(o instanceof Alien)
                {
                    alien = true;
                }
                else if(o instanceof Eau)
                {
                    eau = true;
                }
                else if(o instanceof Minerai)
                {
                    minerai = true;
                }
            }
        }
        /* Si on se trouve sur un alien, l'attaquer */
        if(alien)
        {
            this.attaquer(p);
        }
        else if(this.x == p.getPosVaisseau().getX() && this.y == p.getPosVaisseau().getY())
        {
            this.setRetourFusee(false);
            this.moveInRandomDirection(p);
        }
        else if(retourFusee)
        {
            int i = 0;
            while(i < this.getDistanceDeplacement()
                    && (this.x != p.getPosVaisseau().getX() || this.y != p.getPosVaisseau().getY()))
            {
                this.poserBalise(this.getBalise(),p);
                this.avancerDeUnPasVers(p, p.getVaisseau().getX(),p.getVaisseau().getY());
                i += 1;
            }
        }
        /* Si on se trouve sur du minerai, activer le retour fusée,
           changer le type de balise et commencer à revenir à la fusée
         */
        else if(minerai)
        {
            this.setRetourFusee(true);
            this.setBalise(TypeBalise.MINERAI);
            this.poserBalise(this.getBalise(),p);
            this.avancerDeUnPasVers(p,p.getVaisseau().getX(),p.getVaisseau().getY());
        }
        /* Si on se trouve sur de l'eau, activer le retour fusée,
           changer le type de balise et commencer à revenir à la fusée
         */
        else if(eau)
        {
            this.setRetourFusee(true);
            this.setBalise(TypeBalise.EAU);
            this.poserBalise(this.getBalise(),p);
            this.avancerDeUnPasVers(p,p.getVaisseau().getX(),p.getVaisseau().getY());
        }
        else if( ! gestionAlien(p))
        {
            ArrayList<Location> mineraiEnVue = this.ressourceEnVue(p, Minerai.class);
            ArrayList<Location> eauEnVue = this.ressourceEnVue(p, Eau.class);

            if (!mineraiEnVue.isEmpty())
            {
                Location there = Tools.plusProche(p,new Location(this.x,this.y),mineraiEnVue);
                this.avancerVers(p,there.getX(),there.getY());
            }
            /* Si de l'eau est en vue, y aller */
            else if(!eauEnVue.isEmpty())
            {
                Location there = Tools.plusProche(p,new Location(this.x,this.y),eauEnVue);
                this.avancerVers(p,there.getX(),there.getY());
            }
            /* When all else fails */
            else
            {
                moveInRandomDirection(p);
            }
        }


    }

    /**
     * Pose une balise à l'emplacement du crocosmaute
     * @param t type de la balise posée
     * @param p planète
     */
    public void poserBalise(TypeBalise t, Planete p) {
        Balise b = new Balise(t,p);
        p.getYard().setObjectLocation(b,this.x,this.y);
        Stoppable stoppable = p.schedule.scheduleRepeating(b);
        b.stoppable = stoppable;
    }

    public TypeBalise getBalise() {
        return balise;
    }

    public void setBalise(TypeBalise balise) {
        this.balise = balise;
    }

    public boolean isRetourFusee() {
        return retourFusee;
    }

    public void setRetourFusee(boolean retourFusee) {
        this.retourFusee = retourFusee;
    }
}
