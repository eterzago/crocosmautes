package tutorial.model;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.field.grid.SparseGrid2D;
import sim.util.Int2D;
import tutorial.Tools;
import tutorial.model.crocosmaute.Crocosmaute;
import tutorial.model.crocosmaute.Eclaireur;
import tutorial.model.crocosmaute.Jardinier;
import tutorial.model.crocosmaute.Mineur;
import tutorial.model.environnement.*;

import java.util.ArrayList;

import static tutorial.model.Constants.*;

public class Planete extends SimState {
	public static int NB_DIRECTIONS = 8;
	public SparseGrid2D yard;
	private Location posVaisseau;
	private Vaisseau vaisseau;
	private int nbCroco;
	private int gridSize;
	private ArrayList<Taniere> tanieres;

	public Planete(long seed) {
		super(seed);
	}

	@Override
	public void start() {
		tanieres = new ArrayList<>();
		gridSize = Tools.random(PLANET_SIZE_MIN, PLANET_SIZE_MAX);

		yard = new SparseGrid2D(gridSize,gridSize);

		System.out.println("Simulation started");
		super.start();

		PlaneteManager pm = new PlaneteManager(this);
		Stoppable stoppable = schedule.scheduleRepeating(pm);
		pm.setStoppable(stoppable);


		this.initialiserPlanete(true);
  	}

	/**
	 * Méthode qui génère des ilots de ressources
	 * @param ressource Classe de la ressource à ajouter
	 * @param nombreMaxIlots Nombre maximum d'ilots à ajouter
	 * @param tailleMaxIlot Taille maximum des ilots à ajouter
	 * @throws IllegalArgumentException
	 */
	private void genererPointsRessource(Class ressource, int nombreMaxIlots, int tailleMaxIlot) throws IllegalArgumentException{

		if (ressource.isAssignableFrom(Ressource.class)){
			throw new IllegalArgumentException("Ce n'est pas une ressource !");
		}

		for (int i = 0; i <= nombreMaxIlots; i++){
			Int2D location = getRandomLocation();
			int taillePoint = Tools.random(1, tailleMaxIlot);
			// On ajoute autant de case de ressource que voulu
			for (int j = 1; j <=taillePoint; j++){
				// On cherche une case adjacente libre
				do {
					location = Tools.getRandomLocationAdjacente(this,location);
				} while (!(yard.getObjectsAtLocation(location) == null));

				// On ajoute la ressource dans la case choisie
				Ressource r = RessourceFactory.createRessource(ressource);
				yard.setObjectLocation(r, location.x, location.y);
				r.setX(location.x) ;
				r.setY(location.y) ;
				Stoppable stoppable = schedule.scheduleRepeating(r);
				r.setStoppable(stoppable);
				if(ressource == Taniere.class){
					tanieres.add((Taniere)r);
				}
			}
		}
	}

	/**
	 * Place le vaisseau sur un emplacement aléatoire, et stocke sa référence
	 */
	private void placerVaisseau() {
		Int2D location ;
		do {
			location = getRandomLocation();
		} while ((yard.getObjectsAtLocation(location) != null));

		this.vaisseau = new Vaisseau();
		yard.setObjectLocation(this.vaisseau,location.x,location.y);
		this.vaisseau.setX(location.x);
		this.vaisseau.setY(location.y);
		Stoppable stoppable = schedule.scheduleRepeating(this.vaisseau);
		this.vaisseau.setStoppable(stoppable);
		this.posVaisseau = new Location(this.vaisseau.getX(),this.vaisseau.getY());
	  }

	/**
	 * Génère un équipage initial de crocosmautes à l'emplacement du vaisseau
	 */
	private void genererCrocosmautes(int nombreCrocosmautes) {
		int typeCroco;
		Crocosmaute c;

		this.getVaisseau().viderMinerai();

		/* TODO Pour l'instant y a un nb random de crocos de chaque métier,
		   TODO par la suite faudrait un système de pourcentage, ptet même basé sur
		   TODO les stats générées de chaque crocosmaute (ex. mon croco a une bonne
		   TODO distance de perception -> il sera éclaireur) ?
		 */

		int nbJardiniers = (nombreCrocosmautes * Constants.POURCENTAGE_JARDINIERS)/100;
		int nbMineurs = (nombreCrocosmautes * Constants.POURCENTAGE_MINEURS)/100;
		int nbEclaireurs = (nombreCrocosmautes * Constants.POURCENTAGE_ECLAIREURS)/100;

		if (nbEclaireurs+nbJardiniers+nbMineurs < nombreCrocosmautes){
			nbJardiniers++;
		}


		for(int i = 0; i  < nbJardiniers; i++) {
			int depl = Tools.random(1,Constants.DISTANCE_DEPLACEMENT_MAX);
			int perc = Tools.random(depl, Constants.DISTANCE_PERCEPTION_MAX);
			c = new Jardinier(depl, perc);
			placerCrocosmaute(c);
		}
		for(int i = 0; i  < nbMineurs; i++) {
			int depl = Tools.random(1,Constants.DISTANCE_DEPLACEMENT_MAX);
			int perc = Tools.random(depl, Constants.DISTANCE_PERCEPTION_MAX);
			c = new Mineur(depl, perc);
			placerCrocosmaute(c);
		}
		for(int i = 0; i  < nbEclaireurs; i++) {
			int depl = Tools.random(1,Constants.DISTANCE_DEPLACEMENT_MAX);
			int perc = Tools.random(depl, Constants.DISTANCE_PERCEPTION_MAX);
			c = new Eclaireur(depl, perc);
			placerCrocosmaute(c);
		}
	}

  public void placerCrocosmaute(Crocosmaute c){
	  yard.setObjectLocation(c, posVaisseau.getX(), posVaisseau.getY());
	  c.x = posVaisseau.getX();
	  c.y = posVaisseau.getY();
	  Stoppable stoppable = schedule.scheduleRepeating(c);
	  c.stoppable = stoppable;
	  c.deactivateSignal();
	  vaisseau.inscrireCrocosmaute(c);
  }

  public void attribuerIdEau() {
	  for (Object o : this.yard.getAllObjects()) {
		  if(o instanceof Eau) {
			  String id = "";
			  Location up    = new Location(yard.stx(((Eau)o).getX()), yard.sty(((Eau)o).getY()-1));
			  Location down  = new Location(yard.stx(((Eau)o).getX()), yard.sty(((Eau)o).getY()+1));
			  Location left  = new Location(yard.stx(((Eau)o).getX()-1), yard.sty(((Eau)o).getY()));
			  Location right = new Location(yard.stx(((Eau)o).getX()+1), yard.sty(((Eau)o).getY()));

			  id += (Tools.contientClasse(this,up.getX(),up.getY(),Eau.class))       ? '1' : '0';
			  id += (Tools.contientClasse(this,down.getX(),down.getY(),Eau.class))   ? '1' : '0';
			  id += (Tools.contientClasse(this,left.getX(),left.getY(),Eau.class))   ? '1' : '0';
			  id += (Tools.contientClasse(this,right.getX(),right.getY(),Eau.class)) ? '1' : '0';

			  ((Eau)o).setTileID(id);
		  }
	  }
  }


  boolean estQuittable(){
	return vaisseau.estPretPourLeDepart(this);
  }

	/**
	 * Génère un emplacement aléatoire
	 * @return l'emplacement généré
	 */
  private Int2D getRandomLocation() {
	  return new Int2D(Tools.random(0,yard.getWidth()),
			  Tools.random(0,yard.getHeight()));
	}

	public SparseGrid2D getYard() {
		return yard;
	}

	public Location getPosVaisseau() {
		return posVaisseau;
	}

	public Vaisseau getVaisseau() {
		return vaisseau;
	}

	void initialiserPlanete(boolean firstRun){

  		if (!firstRun){

  			this.nbCroco = this.vaisseau.countCrocosmautes();

			for (Crocosmaute crocosmaute : vaisseau.getCrocosmautes()) {
				crocosmaute.stoppable.stop();
			}

  			this.yard.removeObjectsAtLocation(this.posVaisseau.getX(), this.posVaisseau.getY());
			this.getVaisseau().viderCrocosmautes();
		}

		System.out.println("=== Initializing new Planet ===");
		System.out.println("	- Clearing yard...");
		yard.clear();

		System.out.println("	- Generating ressources...");
		genererPointsRessource(Eau.class, Constants.NOMBRE_LACS_MAX, Constants.TAILLE_LACS_MAX);
		genererPointsRessource(Minerai.class, Constants.NOMBRE_MINES_MAX, Constants.TAILLE_MINES_MAX);
		genererPointsRessource(Fruit.class, Constants.NOMBRE_VERGERS_MAX, Constants.TAILLE_VERGERS_MAX);
		genererPointsRessource(Taniere.class, Constants.NOMBRE_TANIERES_MAX, 1);

		System.out.println("	- Crashing spaceship...");
		placerVaisseau();
		System.out.println("	- Landing crocosmautes...");
		genererCrocosmautes((firstRun)?Constants.EQUIPAGE_INITIAL:(Math.min((new Double(this.nbCroco*1.5)).intValue() , Constants.EQUIPAGE_INITIAL)));
		System.out.println("	- Beautifying seas and oceans...");
		attribuerIdEau();

		System.out.println("	- Planet successfully generated !");

		System.out.println("Crocosmautes left : "+this.nbCroco);

	}

	void cleanTaniere () {
  		tanieres.forEach((t) -> t.cleanAliens(this));
  		tanieres.clear();
	}

	public int getGridSize() {
		return gridSize;
	}

	void setNbCroco(int nbCroco) {
		this.nbCroco = nbCroco;
	}
}
