package tutorial.model;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;

import java.util.Iterator;

public class PlaneteManager implements Steppable {
    Planete planete;
    Stoppable stoppable;

    public PlaneteManager(Planete p ){
        this.planete = p;
    }

    @Override
    public void step(SimState simState) {
        this.planete.attribuerIdEau();
        if (this.planete.estQuittable() && this.planete.getVaisseau().getMinerai() >= Constants.MINERAI_NECESSAIRE){
            Iterator i = this.planete.getYard().allObjects.iterator();
            while(i.hasNext()) {
                Object o = i.next();
                if(o instanceof Creature) {
                    System.out.println("Killing "+o.getClass().getSimpleName());
                    ((Creature)o).stoppable.stop();
                }
                this.planete.yard.remove(o);
            }
            this.planete.setNbCroco(this.planete.getVaisseau().countCrocosmautes());
            this.planete.getVaisseau().stoppable.stop();
            this.planete.cleanTaniere();
            System.out.println("#################################################");
            System.out.println("#####     CHANGEMENT     DE     PLANETE     #####");
            System.out.println("#################################################");
            this.planete.initialiserPlanete(false);
        }
    }

    public void setStoppable(Stoppable stoppable) {
        this.stoppable = stoppable;
    }
}
