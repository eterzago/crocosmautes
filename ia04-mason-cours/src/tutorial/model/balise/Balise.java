package tutorial.model.balise;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import tutorial.model.Constants;
import tutorial.model.Planete;

/**
 * Une balise se comporte comme une trace.
 */
public class Balise implements Steppable {
    public int x,y;
    private TypeBalise type;
    private int intensite;
    public Stoppable stoppable;

    public Balise(TypeBalise t, Planete p) {
        this.type = t;
        this.intensite = Balise.getIntensiteMax(p);
    }

    @Override
    public void step(SimState simState) {
        Planete p = (Planete)simState;
        this.intensite -= 1;
        if(this.intensite <= 0) {
            p.yard.remove(this);
            this.stoppable.stop();
        }
    }

    public TypeBalise getType() {
        return type;
    }

    public int getIntensite() {
        return intensite;
    }

    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName() + "] "
                + "t=" + this.intensite;
    }

    public static int getIntensiteMax(Planete p) {
        return p.getGridSize()/2 + 1;
    }
}
