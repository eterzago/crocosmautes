package tutorial.model.environnement;

public class RessourceFactory {
    public static Ressource createRessource(Class<? extends Ressource> clazz){
        if (clazz.equals(Eau.class)){
            return new Eau();
        }
        if (clazz.equals(Minerai.class)){
            return new Minerai();
        }
        if (clazz.equals(Taniere.class)){
            return new Taniere();
        }
        if (clazz.equals(Fruit.class)){
            return new Fruit();
        }
        return null;
    }
}
