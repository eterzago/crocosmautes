package tutorial.model.environnement;

import sim.engine.SimState;
import sim.engine.Steppable;

public class Obstacle implements Steppable {
    private int x,y;

    @Override
    public void step(SimState simState) {
        // Cool sa vie
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "["+this.x + "," + this.y+"]";
    }
}
