package tutorial.model.environnement;
import tutorial.Tools;
import tutorial.model.Constants;

import java.util.Random;

public class Minerai extends Ressource {
    Minerai() {
        super();
        this.setQuantite(Tools.random(1,Constants.DURETE_MINERAI_MAX));
    }
}
