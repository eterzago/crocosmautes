package tutorial.model.environnement;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import tutorial.Tools;
import tutorial.model.Constants;
import tutorial.model.Planete;

import java.util.Random;

public class Fruit extends Ressource {
    private int etat;          /* Croissance de la plante */
    private boolean estArrose; /* Condition pour la croissance à l'état suivant */
    private int clock;         /* Prend 3 pas d'horloge pour passer à l'état suivant */

    Fruit()
    {
        super();
        this.etat = Constants.ETAT_FRUIT;
        this.setQuantite(1);
        this.setEstArrose(false);
        this.clock = 0;
    }

    @Override
    public void step(SimState simState) {
        Planete planete = (Planete) simState;
        if (this.getQuantite() <= 0)
        {
            planete.yard.remove(this);
            this.stoppable.stop();
        }
        else
        {
            /*
              Machine à états correspondant à la croissance de la plante
             */
            switch (this.clock)
            {
                case 0:
                case 1:
                    this.clock ++;
                    break;
                case 2:
                    switch(etat)
                    {
                        case Constants.ETAT_FRUIT:
                        case Constants.ETAT_POUSSE:
                            if(estArrose) {
                                /* Reset l'horloge */
                                this.clock = 0;
                                /* Grandit */
                                this.etat ++;
                                /* A de nouveau besoin d'eau */
                                this.estArrose = false;
                            }
                            break;
                        case Constants.ETAT_ARBRISSEAU:
                            if(estArrose) {
                                /* Reset l'horloge */
                                this.clock = 0;
                                /* Grandit */
                                this.etat ++;
                                /* Produit un ou plusieurs fruits */
                                this.produireFruits();
                            }
                            break;
                        default:
                            break;
                    }
                    break;
            }

        }
    }


    /**
     * Produit une quantité variable de fruits
     */
    private void produireFruits() {
        this.setQuantite(Tools.random(Constants.NB_FRUITS_MIN, Constants.NB_FRUITS_MAX));
    }

    /**
     * Décrémente le nombre de fruits présents sur l'arbre
     */
    public void cueillirFruit() {
        this.setQuantite(this.getQuantite() - 1);
    }

    public int getEtat() {
        return etat;
    }

    public boolean isEstArrose() {
        return estArrose;
    }

    public void setEstArrose(boolean estArrose) {
        this.estArrose = estArrose;
    }

    @Override
    public String toString() {
        return super.toString() + " eau=" + (this.isEstArrose() ? "y" : "n");
    }
}
