package tutorial.model.environnement;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import tutorial.model.Location;
import tutorial.model.Planete;

public abstract class Ressource implements Steppable {
    private int quantite;
    protected Location location;
    Stoppable stoppable;

    Ressource() {
        this.location = new Location(-1, -1);
    }

    @Override
    public void step(SimState simState) {
        Planete p = (Planete) simState;
        if(this.getQuantite() <= 0) {
            p.yard.remove(this);
            this.stoppable.stop();
        }
    }

    /**
     * Décrémente la quantité restante dans l'unité de ressource.
     */
    public void decrementerQuantite(int quantite) {
        this.setQuantite(this.getQuantite() - quantite);
    }


    public int getX() {
        return this.location.getX();
    }

    public int getY() {
        return this.location.getY();
    }

    public void setX(int x) {
        this.location.setX(x);
    }

    public void setY(int y) {
        this.location.setY(y);
    }

    public void setStoppable(Stoppable stoppable) {
        this.stoppable = stoppable;
    }

    public int getQuantite() {
        return quantite;
    }

    void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "["+this.getX() + "," + this.getY()+"]"+ " " + "q=" + this.getQuantite();
    }
}
