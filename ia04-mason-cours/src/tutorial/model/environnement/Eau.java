package tutorial.model.environnement;
import tutorial.Tools;
import tutorial.model.Constants;

public class Eau extends Ressource {
    private String tileID;

    Eau() {
        super();
        this.setQuantite(Tools.random(1,Constants.QUANTITE_EAU_MAX));
    }

    public String getTileID() {
        return tileID;
    }

    public void setTileID(String tileID) {
        this.tileID = tileID;
    }
}
