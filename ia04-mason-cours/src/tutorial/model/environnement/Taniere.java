package tutorial.model.environnement;

import sim.engine.SimState;
import sim.engine.Stoppable;
import tutorial.Tools;
import tutorial.model.Constants;
import tutorial.model.Planete;
import tutorial.model.alien.Alien;
import tutorial.model.alien.AlienCarnivore;
import tutorial.model.alien.AlienFactory;
import tutorial.model.alien.AlienHerbivore;

import java.util.ArrayList;

/**
 * Classe qui définit une Taniere d'Alien
 */
public class Taniere extends Ressource {
    private int populationBuffer;
    private Class alienType;
    private ArrayList<Alien> aliens;

    Taniere(){
        super();
        aliens = new ArrayList<>();
        this.populationBuffer = Constants.NOMBRE_ALIENS_PAR_TANIERE;
        this.alienType = (Tools.random(0,1) == 1) ?
                AlienCarnivore.class : AlienHerbivore.class;
    }

    @Override
    public void step(SimState simState) {
        Planete p = (Planete)simState;

        if (this.populationBuffer > 0){
            this.decrementerPopulationBuffer(1);

            Alien r = AlienFactory.createAlien(this.alienType, this.getX(), this.getY());
            p.yard.setObjectLocation(r, this.getX(), this.getY());
            Stoppable stoppable = p.schedule.scheduleRepeating(r);
            r.setStoppable(stoppable);
            aliens.add(r);
        }
    }

    public void cleanAliens (Planete p) {
        aliens.forEach((a) -> {
            a.stoppable.stop();
            p.yard.remove(a);
        });
    }

    private void decrementerPopulationBuffer(int i) {
        this.setPopulationBuffer(this.getPopulationBuffer() - i);
    }

    private int getPopulationBuffer() {
        return populationBuffer;
    }

    private void setPopulationBuffer(int populationBuffer) {
        this.populationBuffer = populationBuffer;
    }
}
