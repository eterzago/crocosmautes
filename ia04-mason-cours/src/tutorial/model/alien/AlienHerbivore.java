package tutorial.model.alien;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import sim.engine.SimState;
import sim.util.Bag;
import tutorial.Tools;
import tutorial.model.Constants;
import tutorial.model.Location;
import tutorial.model.Planete;
import tutorial.model.balise.Balise;
import tutorial.model.crocosmaute.Crocosmaute;
import tutorial.model.environnement.Fruit;
import tutorial.model.environnement.Ressource;

import java.util.ArrayList;
import java.util.function.Predicate;

public class AlienHerbivore extends Alien {

    AlienHerbivore(int x, int y) {
        super(x,y);
    }

    @Override
    public void step(SimState simState) {
        Planete p = (Planete) simState;
        this.location.setX(this.x);
        this.location.setY(this.y);
        if(this.vie <= 0) {
            p.yard.remove(this);
            stoppable.stop();
        }
        /* Si on se trouve sur une plante, la manger */
        else if(Tools.contientClasse(p, this.getX(), this.getY(), Fruit.class)
                && ((Fruit)(Tools.getRessource(p,this.x,this.y,Fruit.class))).getEtat() == Constants.ETAT_ARBRE) {
            // manger si on est sur une plante
            this.mangerPlante(p);
        }
        else {
            ArrayList<Location> plantes =ressourceEnVue(p, Fruit.class);
            Tools.filtrerPlantes(p, plantes,Constants.ETAT_ARBRE);
            ArrayList<Location> balises =ressourceEnVue(p, Balise.class);
            ArrayList<Location> crocosmautes =ressourceEnVue(p, Crocosmaute.class);
            ArrayList<Location> carnivores =ressourceEnVue(p, AlienCarnivore.class);
            /* Si des plantes sont en vue, s'y rendre */
            if (!plantes.isEmpty()) {
                Location planteProche = Tools.plusProche(p,this.location,plantes);
                this.avancerVers(p,planteProche.getX(),planteProche.getY());
            }
            /* Si des balises sont en vue, les suivre */
            else if (! balises.isEmpty()) {
                Location baliseDest = this.directionBalises(balises);
                this.avancerVers(p,baliseDest.getX(),baliseDest.getY());
            }
            /* Si on est sur un croco, le fuir */
            else if (Tools.contientClasse(p, this.getX(), this.getY(), Crocosmaute.class)) {
                // fuir si on est sur la même case qu'un croco
                this.fuir(p,new Location(this.getX(),this.getY()));
            }
            /* Sinon si des crocos sont en vue */
            else if (! crocosmautes.isEmpty()){
                /* Si des carnivores sont également en vue, yolo */
                if (! carnivores.isEmpty()) {
                    this.moveInRandomDirection(p);
                }
                else {
                    Location croco = Tools.plusProche(p,this.location,crocosmautes);
                    /* Si trop proche d'un croco, le fuir */
                    if (tropProche(location, croco)) {
                        this.fuir(p, croco);
                    }
                    /* Suivre subrepticement le croco */
                    else {
                        this.avancerMaisPasTrop(p, croco);
                    }
                }
            } else {
                this.moveInRandomDirection(p);
            }
        }
    }

    /**
     * Mange la plante qui se trouve sur l'emplacement de l'alien
     * @param p planète
     */
    private void mangerPlante(Planete p) {
        Bag b = p.getYard().getObjectsAtLocation(this.getX(),this.getY());
        if(b != null) {
            for (Object o : b) {
                if (o instanceof Fruit) {
                    ((Fruit)o).cueillirFruit();
                }
            }
        }
    }

    private void filtrerFruits(Planete p, ArrayList<Location> plantes, int etat){
        plantes.removeIf(loc -> ! (((Fruit)Tools.getRessource(p,loc.getX(),loc.getY(),Fruit.class)).getEtat() == Constants.ETAT_ARBRE));
    }

    // avancer à l'opposer du crocosmaute, de préférence vers la tanière
    private void fuir (Planete p, Location aEviter) {
        int directionX = this.getX() - aEviter.getX();
        if (directionX != 0) {
            directionX /= Math.abs(this.getX() - aEviter.getX());
        } else {
            directionX = this.posTaniere.getX() - this.getX();
            if (directionX != 0) {
                directionX /= Math.abs(this.posTaniere.getX() - this.getX());
            } else {
                directionX = (Tools.random(0,1)==0)?-1:1;
            }
        }
        int directionY = this.getY() - aEviter.getY();
        if (directionY != 0) {
            directionY /= Math.abs(this.getY() - aEviter.getY());
        } else {
            directionY = this.posTaniere.getY() - this.getY();
            if (directionY != 0) {
                directionY /= Math.abs(this.posTaniere.getY() - this.getY());
            } else {
                directionY = (Tools.random(0,1)==0)?-1:1;
            }
        }
        avancerVers(p, ((Math.abs(this.getX()) + this.distanceDeplacement) * directionX), ((Math.abs(this.getY()) + this.distanceDeplacement) * directionY));
    }

    private boolean tropProche (Location moi, Location aEviter) {
        return (moi.getX() < aEviter.getX() + Constants.ALIEN_MIN_A_EVITER && moi.getX() > aEviter.getX() - Constants.ALIEN_MIN_A_EVITER)
                && (moi.getY() < aEviter.getY() + Constants.ALIEN_MIN_A_EVITER && moi.getY() > aEviter.getY() - Constants.ALIEN_MIN_A_EVITER);
    }

    private void avancerMaisPasTrop (Planete p,Location aEviter) {
        // ne pas aller vers le aEviter, mais s'en approcher pour aller derrière lui
        // essayer de deviner où il va en fonction de son type  et des alentours
        int xDirection, yDirection;
        if (aEviter.getX() == this.getX()){
            xDirection = (Tools.random(0,1)==0)?-1:1;
        } else if (aEviter.getX() < this.getX()) {
            xDirection = -1;
        } else {
            xDirection = 1;
        } if (aEviter.getY() == this.getY()){
            yDirection = (Tools.random(0,1)==0)?-1:1;
        } else if (aEviter.getY() < this.getY()) {
            yDirection = -1;
        } else {
            yDirection = 1;
        }
        int x = 0,y=0;
        for (int cpt=0; cpt<this.distanceDeplacement; cpt++) {
            if (!tropProche(new Location(this.getX() + (x + 1) * xDirection, this.getY() + (y + 1) * yDirection), aEviter)) {
                x++;
                y++;
            } else if (!tropProche(new Location(this.getX() + (x + 1) * xDirection, this.getY() + y * yDirection), aEviter)) {
                x++;
            } else {
                y++;
            }
        }
        avancerVers(p,this.getX() + x * xDirection,this.getY() + y * yDirection);
    }
}
