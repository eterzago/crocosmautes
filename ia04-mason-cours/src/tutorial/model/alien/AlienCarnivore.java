package tutorial.model.alien;

import sim.engine.SimState;
import sim.util.Bag;
import tutorial.Tools;
import tutorial.model.Constants;
import tutorial.model.Location;
import tutorial.model.Planete;
import tutorial.model.Vaisseau;
import tutorial.model.balise.Balise;
import tutorial.model.crocosmaute.Crocosmaute;
import tutorial.model.environnement.Fruit;

import java.util.ArrayList;

public class AlienCarnivore extends Alien {

    AlienCarnivore(int x, int y){
        super(x,y);
    }

    @Override
    public void step(SimState simState) {
        Planete p = (Planete) simState;
        this.location.setX(this.x);
        this.location.setY(this.y);
        if(this.vie <= 0) {
            p.yard.remove(this);
            stoppable.stop();
        }
        /* Si on est sur la même case qu'un croco, l'attaquer */
        else if(Tools.contientClasse(p, this.getX(), this.getY(), Crocosmaute.class)) {
            this.attaquerCroco(p);
        } else {
            ArrayList<Location> balises =ressourceEnVue(p, Balise.class);
            ArrayList<Location> crocosmautes =ressourceEnVue(p, Crocosmaute.class);
            ArrayList<Location> herbivores =ressourceEnVue(p, AlienHerbivore.class);
            ArrayList<Location> vaisseau =ressourceEnVue(p, Vaisseau.class);
            /* Avancer vers le croco le plus proche */
            if (! crocosmautes.isEmpty()) {
                Location croco = Tools.plusProche(p, this.location, crocosmautes);
                avancerVers(p,croco.getX(),croco.getY());
            }
            /* Protéger et suivre les herbivores en priorité */
            else if (! herbivores.isEmpty()) {
                Location herb = Tools.plusProche(p,this.location, herbivores);
                avancerVers(p,herb.getX(), herb.getY());
            }
            /* Il y a toujours des crocos non loin du vaisseau */
            else if( ! vaisseau.isEmpty() ) {
                Location v = Tools.plusProche(p,this.location, vaisseau);
                avancerVers(p,v.getX(), v.getY());
            }
            /* Suivre les chemins de balises pour aller vers le vaisseau */
            else if (! balises.isEmpty()) {
                Location baliseDest = this.directionBalises(balises);
                this.avancerVers(p,baliseDest.getX(),baliseDest.getY());
            }
            /* Rien de spécial à analyser, on avance au hasard */
            else {
                this.moveInRandomDirection(p);
            }
        }
    }

    private void attaquerCroco (Planete p) {
        Bag b = p.getYard().getObjectsAtLocation(this.getX(),this.getY());
        if(b != null) {
            for (Object o : b) {
                if (o instanceof Crocosmaute) {
                    ((Crocosmaute)o).frapperCroco(Tools.random(Constants.ALIEN_MIN_ATTAQUE,Constants.ALIEN_MAX_ATTAQUE));
                }
            }
        }
    }
}
