package tutorial.model.alien;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import tutorial.Tools;
import tutorial.model.Constants;
import tutorial.model.Location;
import tutorial.model.Creature;
import tutorial.model.Planete;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

public abstract class Alien extends Creature {
    protected Location location, posTaniere;
    protected int vie;

    Alien(int x, int y) {
        this.location = new Location(x, y);
        this.posTaniere = new Location(x, y);
        this.vie = Tools.random(Constants.ALIEN_VIE_MIN, Constants.ALIEN_VIE_MAX);
        this.distanceDeplacement = Tools.random(1,Constants.ALIEN_DISTANCE_DEPLACEMENT_MAX);
        this.distancePerception = Tools.random(distanceDeplacement,Constants.ALIEN_DISTANCE_PERCEPTION_MAX);
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setStoppable(Stoppable stoppable) {
        this.stoppable = stoppable;
    }

    public void decrementVie(int vieEnleve) {
        vie -= vieEnleve;
    }

    protected boolean estVeteran () {
        //TODO: regarde si l'alien est le veteran du groupe
        return false;
    }

    protected boolean veteranAutour () {
        //TODO, alien avec le moins de pv
        return true;
    }

    protected void suivreVeteran () {
        //TODO, deplacement vers le veteran
    }


    // vers où aller en fonction des balises
    protected Location directionBalises (ArrayList<Location> balises) {
        // on va vers la balise la plus loin => surement en bord de perception
        Location balisePlusLoin = balises.get(0);
        for (Location b:balises) {
            if(distPoint(this.location, b) > distPoint(this.location, balisePlusLoin)) {
                balisePlusLoin = b;
            }
        }
        return balisePlusLoin;
    }

    protected int distPoint (Location moi, Location aEviter) {
        int dist = Math.abs(Math.abs(moi.getX()) - Math.abs(aEviter.getX()) - Constants.ALIEN_MIN_A_EVITER);
        dist += Math.abs(Math.abs(moi.getY()) - Math.abs(aEviter.getY()) - Constants.ALIEN_MIN_A_EVITER);
        return dist;
    }

    public void frapperAlien (int nbDegats) {
        this.vie -= nbDegats;
    }
}
