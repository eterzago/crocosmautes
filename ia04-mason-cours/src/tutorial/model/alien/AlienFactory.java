package tutorial.model.alien;

public class AlienFactory {
    public static Alien createAlien(Class<? extends Alien> alienClass, int x, int y){
        if (alienClass.equals(AlienHerbivore.class)){
            return new AlienHerbivore(x,y);
        }
        if (alienClass.equals(AlienCarnivore.class)){
            return new AlienCarnivore(x,y);
        }
        return null;
    }
}
