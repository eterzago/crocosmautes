package tutorial.model;

public class Constants {
    public static final int PLANET_SIZE_MIN = 40;
    public static final int PLANET_SIZE_MAX = 50;

    public static final int NOMBRE_LACS_MAX = 10;
    public static final int TAILLE_LACS_MAX = 40;
    public static final int NOMBRE_MINES_MAX = 15;
    //public static final int TAILLE_MINES_MIN = 6;
    public static final int TAILLE_MINES_MAX = 10;
    public static final int NOMBRE_VERGERS_MAX = 10;
    public static final int TAILLE_VERGERS_MAX = 10;

    public static final int NOMBRE_TANIERES_MAX = 3;
    public static final int NOMBRE_ALIENS_PAR_TANIERE = 2;

    public final static int EQUIPAGE_INITIAL = 10;
    public final static int POURCENTAGE_JARDINIERS= 35;
    public final static int POURCENTAGE_MINEURS= 50;
    public final static int POURCENTAGE_ECLAIREURS= 15;

    public final static int MINERAI_NECESSAIRE = 20;
    public final static int STOCK_OXYGENE = 20;
    public final static int STOCK_FRUITS = 40;
    public final static int RATIO_MINERAI_OXYGENE = 2;

    public final static int OXYGENE_MAX = 30;
    public final static int SATIETE = 100;
    public final static int SEUIL_CRITIQUE_SATIETE = 10;
    public final static int SEUIL_CRITIQUE_OXYGENE = 10;
    public final static int MARGE_OXYGENE = 5;
    public final static int MARGE_SATIETE = 5;
    public final static int CHARGE_FRUITS_MAX = 20;
    public final static int CHARGE_FRUITS_CRITIQUE = 5;
    public final static int CHARGE_MINERAI_MAX = 2;
    public final static int CHARGE_EAU_MAX = 8;
    public final static int DISTANCE_DEPLACEMENT_MAX = 4;
    public final static int DISTANCE_PERCEPTION_MAX = 8;
    public final static int ATTAQUE_CROCO = 1;

    public final static int ALIEN_VIE_MAX = 5;
    public final static int ALIEN_VIE_MIN = 2;
    public final static int ALIEN_DISTANCE_DEPLACEMENT_MAX = 3;
    public final static int ALIEN_DISTANCE_PERCEPTION_MAX = 8;
    public final static int ALIEN_MIN_A_EVITER = 3;
    public final static int ALIEN_MIN_ATTAQUE = 2;
    public final static int ALIEN_MAX_ATTAQUE = 3;

    public final static int ETAT_FRUIT = 0;
    public final static int ETAT_POUSSE = 1;
    public final static int ETAT_ARBRISSEAU = 2;
    public final static int ETAT_ARBRE = 3;
    public final static int NB_FRUITS_MIN = 3;
    public final static int NB_FRUITS_MAX = 6;
    public final static int NUTRITION_FRUIT = 50;

    public final static int QUANTITE_EAU_MAX = 15;
    public final static int DURETE_MINERAI_MAX = 3;
}
