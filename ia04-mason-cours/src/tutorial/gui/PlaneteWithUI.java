package tutorial.gui;

import java.awt.*;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.portrayal.DrawInfo2D;
import sim.portrayal.Inspector;
import sim.portrayal.grid.SparseGridPortrayal2D;
import sim.portrayal.simple.ImagePortrayal2D;
import sim.portrayal.simple.OvalPortrayal2D;
import tutorial.model.*;
import tutorial.model.alien.AlienCarnivore;
import tutorial.model.alien.AlienHerbivore;
import tutorial.model.balise.Balise;
import tutorial.model.crocosmaute.Eclaireur;
import tutorial.model.crocosmaute.Jardinier;
import tutorial.model.crocosmaute.Mineur;
import tutorial.model.environnement.*;

public class PlaneteWithUI extends GUIState {
	public static int FRAME_SIZE = 600;
	public Display2D display;
	public JFrame displayFrame;
	SparseGridPortrayal2D yardPortrayal = new SparseGridPortrayal2D();
	
	public PlaneteWithUI(SimState state) {
		super(state);
	}

	public static String getName() {
		return "Simulation de crocosmautes";
	}
	public void start() {
	  super.start();
	  setupPortrayals();
	}

	public void load(SimState state) {
	  super.load(state);
	  setupPortrayals();
	}
	public void setupPortrayals() {
	  Planete planete = (Planete) state;
	  yardPortrayal.setField(planete.yard);
	  yardPortrayal.setPortrayalForClass(Jardinier.class, getCrocosmautePortrayal());
	  yardPortrayal.setPortrayalForClass(Mineur.class, getCrocosmautePortrayal());
	  yardPortrayal.setPortrayalForClass(Eclaireur.class, getCrocosmautePortrayal());
	  yardPortrayal.setPortrayalForClass(Vaisseau.class, getVaisseauPortrayal());
	  yardPortrayal.setPortrayalForClass(Fruit.class, getFruitPortrayal());
	  yardPortrayal.setPortrayalForClass(Eau.class, getEauPortrayal(planete));
	  yardPortrayal.setPortrayalForClass(Minerai.class, getMineraiPortrayal());
	  yardPortrayal.setPortrayalForClass(Balise.class, getBalisePortrayal());
	  yardPortrayal.setPortrayalForClass(Obstacle.class, getObstaclePortrayal());
	  yardPortrayal.setPortrayalForClass(AlienHerbivore.class, getAlienHerbivorePortrayal());
	  yardPortrayal.setPortrayalForClass(AlienCarnivore.class, getAlienCarnivorePortrayal());
	  yardPortrayal.setPortrayalForClass(Taniere.class, getTanierePortrayal());
	  display.reset();
	  Paint paint = Color.getHSBColor(25,69,42);
	  display.setBackdrop(paint);
	  //addBackgroundImage();
	  display.repaint();
	}

	private ImagePortrayal2D getAlienHerbivorePortrayal() {
		return new ImagePortrayal2D(new ImageIcon("./res/img/alien_herbivore.png"));
	}
	private ImagePortrayal2D getAlienCarnivorePortrayal() {
		return new ImagePortrayal2D(new ImageIcon("./res/img/alien_carnivore.png"));
	}

	private ImagePortrayal2D getCrocosmautePortrayal() {
		ImagePortrayal2D p = new CrocosmautePortrayal(new ImageIcon("./res/img/croco_jardinier.png"));
		return p;
	}

	private ImagePortrayal2D getEauPortrayal(Planete planete) {
		WaterPortrayal p = new WaterPortrayal(new ImageIcon("./res/img/eau.png"),planete);
		return p;
	}
	private ImagePortrayal2D getMineraiPortrayal() {
		MineraiPortrayal p = new MineraiPortrayal(new ImageIcon("./res/img/minerai.png"));
		return p;
	}

	private ImagePortrayal2D getVaisseauPortrayal()
	{
		ImagePortrayal2D p = new ImagePortrayal2D(new ImageIcon("./res/img/fusee.png"));
		return p;
	}
	private ImagePortrayal2D getFruitPortrayal()
	{
		ImagePortrayal2D p = new FruitPortrayal(new ImageIcon("./res/img/fruit.png"));
		return p;
	}
	private ImagePortrayal2D getObstaclePortrayal()
	{
		ImagePortrayal2D p = new ImagePortrayal2D(new ImageIcon("./res/img/obstacle.png"));
		return p;
	}

	private ImagePortrayal2D getBalisePortrayal() {
		return new BalisePortrayal(new ImageIcon("./res/img/balise_eau.png"));
	}

	private ImagePortrayal2D getTanierePortrayal() {
		return new ImagePortrayal2D(new ImageIcon("./res/img/taniere.png"));
	}

	/*private OvalPortrayal2D getBalisePortrayal()
	{
		OvalPortrayal2D p = new OvalPortrayal2D()
		{
			public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
			{
				Balise b = (Balise)object;
				switch(b.getType()) {
					case EAU:
						paint = Color.CYAN;
						break;
					case ALIEN:
						paint = Color.GREEN;
						break;
					case MINERAI:
						paint = Color.DARK_GRAY;
						break;
					default:
						paint = Color.MAGENTA; // If it's pink, there's a problem
				}
				super.draw(object, graphics, info);
			}
		};
		p.filled = true;
		p.scale = 0.5;
		return p;
	}*/



	public void init(Controller c) {
		  super.init(c);
		  display = new Display2D(FRAME_SIZE,FRAME_SIZE,this);
		  display.setClipping(false);
		  displayFrame = display.createFrame();
		  displayFrame.setTitle("Planete");
		  c.registerFrame(displayFrame); // so the frame appears in the "Display" list
		  displayFrame.setVisible(true);
		  display.attach( yardPortrayal, "Yard" );
		}
	private void addBackgroundImage() {
	  Image i = new ImageIcon(getClass().getResource("grass.png")).getImage();
	  int w = i.getWidth(null)/5;
	  int h = i.getHeight(null)/5;
	  BufferedImage b = display.getGraphicsConfiguration().createCompatibleImage(w,h);
	  Graphics g = b.getGraphics();
	  g.drawImage(i,0,0,w,h,null);
	  g.dispose();
	  display.setBackdrop(new TexturePaint(b, new Rectangle(0,0,w,h)));
	}
	public  Object  getSimulationInspectedObject()  {  return  state;  }
	public  Inspector  getInspector() {
	Inspector  i  =  super.getInspector();
	  i.setVolatile(true);
	  return  i;
	}
}
