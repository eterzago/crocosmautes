package tutorial.gui;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.simple.ImagePortrayal2D;
import tutorial.model.Constants;
import tutorial.model.environnement.Eau;
import tutorial.model.environnement.Minerai;

import javax.swing.*;
import java.awt.*;

public class MineraiPortrayal extends ImagePortrayal2D {
    private ImageIcon minerai = new ImageIcon("./res/img/minerai.png");
    private ImageIcon mineraiRare = new ImageIcon("./res/img/minerai_rare.png");


    public MineraiPortrayal(ImageIcon icon) {
        super(icon);
    }

    @Override
    public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
        Minerai m = (Minerai) object;
        int quantite = m.getQuantite();
        if (quantite >= (Constants.DURETE_MINERAI_MAX + 1) / 2) {
            this.image = mineraiRare.getImage();
        } else this.image = minerai.getImage();


        super.draw(object, graphics, info);
    }
}
