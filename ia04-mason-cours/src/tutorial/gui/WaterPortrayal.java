package tutorial.gui;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.simple.ImagePortrayal2D;
import tutorial.Tools;
import tutorial.model.Constants;
import tutorial.model.Location;
import tutorial.model.Planete;
import tutorial.model.environnement.Eau;
import javax.swing.*;
import java.awt.*;

public class WaterPortrayal extends ImagePortrayal2D {
    private Planete planete;
    private String imgPath = "./res/img/eau/";
    private ImageIcon eau = new ImageIcon("./res/img/eau.png");
    private ImageIcon eauProfonde = new ImageIcon("./res/img/eau_profonde.png");


    public WaterPortrayal(ImageIcon icon,Planete p) {
        super(icon);
        this.planete = p;
    }

    @Override
    public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
        Eau e = (Eau)object;

        String path = this.imgPath;
        path += (e.getQuantite() >= (Constants.QUANTITE_EAU_MAX + 1)/2) ? "profonde/" : "";

        this.image = new ImageIcon(
                path + "eau_" + e.getTileID() + ".png"
        ).getImage();

        super.draw(object, graphics, info);
    }

    /**
     * Retourne la chaîne binaire correspondant à l'identifiant
     * du tile qui correspond à l'emplacement d'eau donné
     * @param p planète
     * @param l emplacement de la case d'eau
     * @return l'identifiant du bon tile
     */
    private String getTileID(Planete p, Location l){
        String id = "";
        Location up    = new Location(p.yard.stx(l.getX()), p.yard.sty(l.getY()-1));
        Location down  = new Location(p.yard.stx(l.getX()), p.yard.sty(l.getY()+1));
        Location left  = new Location(p.yard.stx(l.getX()-1), p.yard.sty(l.getY()));
        Location right = new Location(p.yard.stx(l.getX()+1), p.yard.sty(l.getY()));

        id += (Tools.contientClasse(p,up.getX(),up.getY(),Eau.class))       ? '1' : '0';
        id += (Tools.contientClasse(p,down.getX(),down.getY(),Eau.class))   ? '1' : '0';
        id += (Tools.contientClasse(p,left.getX(),left.getY(),Eau.class))   ? '1' : '0';
        id += (Tools.contientClasse(p,right.getX(),right.getY(),Eau.class)) ? '1' : '0';

        System.out.println("tile "+id);
        return id;
    }

    /*@Override
    public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
        Eau e = (Eau)object;
        int quantite = e.getQuantite();
        if(quantite >= (Constants.QUANTITE_EAU_MAX + 1)/2) {
            this.image = eauProfonde.getImage();
        }
        else this.image = eau.getImage();

        super.draw(object, graphics, info);
    }*/
}
