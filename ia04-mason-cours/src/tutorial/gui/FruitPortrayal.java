package tutorial.gui;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.simple.ImagePortrayal2D;
import tutorial.model.Constants;
import tutorial.model.environnement.Fruit;

import javax.swing.*;
import java.awt.*;

public class FruitPortrayal extends ImagePortrayal2D {


    public FruitPortrayal(ImageIcon icon) {
        super(icon);
    }

    @Override
    public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
        String path = "./res/img/";
        Fruit f = (Fruit)object;
        int etat = f.getEtat();
        switch(etat) {
            case Constants.ETAT_FRUIT:
                path += "fruit";
                break;
            case Constants.ETAT_POUSSE:
                path += "pousse";
                break;
            case Constants.ETAT_ARBRISSEAU:
                path += "arbrisseau";
                break;
            case Constants.ETAT_ARBRE:
                path += "arbre";
                break;
        }
        if(!(f.isEstArrose()) && f.getEtat() != Constants.ETAT_ARBRE) {
            path += "_sec";
        }
        path += ".png";
        this.image = new ImageIcon(path).getImage();
        super.draw(object, graphics, info);
    }
}
