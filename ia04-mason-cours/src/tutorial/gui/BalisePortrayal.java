package tutorial.gui;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.simple.ImagePortrayal2D;
import tutorial.model.balise.Balise;

import javax.swing.*;
import java.awt.*;

public class BalisePortrayal extends ImagePortrayal2D {
    private ImageIcon bEau = new ImageIcon("./res/img/balise_eau.png");
    private ImageIcon bAlien = new ImageIcon("./res/img/balise_alien.png");
    private ImageIcon bMinerai = new ImageIcon("./res/img/balise_minerai.png");

    public BalisePortrayal(ImageIcon icon) {
        super(icon);
    }

    @Override
    public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
        Balise b = (Balise)object;
        switch(b.getType()) {
            case EAU:
                this.image = this.bEau.getImage();
                break;
            case ALIEN:
                this.image = this.bAlien.getImage();
                break;
            case MINERAI:
                this.image = this.bMinerai.getImage();
                break;
        }
        super.draw(object, graphics, info);
    }
}
