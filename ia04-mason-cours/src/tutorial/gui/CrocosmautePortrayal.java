package tutorial.gui;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.simple.ImagePortrayal2D;
import tutorial.model.crocosmaute.Crocosmaute;
import tutorial.model.crocosmaute.Eclaireur;
import tutorial.model.crocosmaute.Jardinier;
import tutorial.model.crocosmaute.Mineur;

import javax.swing.*;
import java.awt.*;

public class CrocosmautePortrayal extends ImagePortrayal2D {

    private ImageIcon jardinier = new ImageIcon("./res/img/croco_jardinier.png");
    private ImageIcon jardinierFruits = new ImageIcon("./res/img/croco_jardinier_fruits.png");
    private ImageIcon mineur = new ImageIcon("./res/img/croco_mineur.png");
    private ImageIcon mineurMinerai = new ImageIcon("./res/img/croco_mineur_minerai.png");
    private ImageIcon eclaireur = new ImageIcon("./res/img/croco_eclaireur.png");


    public CrocosmautePortrayal(ImageIcon icon) {
        super(icon);
    }

    @Override
    public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
        Crocosmaute c = (Crocosmaute)object;
        if(c instanceof Jardinier)
        {
            if(c.getChargeFruits() > 0)
            {
                this.image = jardinierFruits.getImage();
            }
            else this.image = jardinier.getImage();
        }
        else if(c instanceof Mineur)
        {
            if(((Mineur)c).getChargeMinerai() > 0)
            {
                this.image = mineurMinerai.getImage();
            }
            else this.image = mineur.getImage();
        }
        else if (c instanceof Eclaireur)
        {
            this.image = eclaireur.getImage();
        }

        super.draw(object, graphics, info);
    }
}
