# IA04 - Simulation de survie de crocosmautes dans un environnement hostile

Un vaisseau s'écrase sur une planète inconnue. Les crocosmautes explorent les lieux et doivent tenter de réparer leur vaisseau afin de pouvoir repartir.

Pour réparer le vaisseau, il faut un certain nombre d'unités de minerai ; le vaisseau produit de l'oxygène pour chaque unité qu'il consomme, et une fois le nombre d'unités requises atteint, la fusée peut repartir.

Pour cela, ils possèdent un stock limité de graines dans le vaisseau. chaque cosmonaute peut récupérer un fruit en allant au vaisseau, fruit qu'il a la possibilité de manger pour assurer sa survie ou de planter pour renouveler le stock de fruits. Un fruit planté a besoin d'eau afin de croître et de donner à son tour d'autres fruits, qui seront ensuite récoltés par les crocosmautes et acheminés au vaisseau. Heureusement pour les crocosmautes, des sources d'eau sont présentes sur la planète.

Chaque cosmonaute possède en outre une bouteille d'oxygène, essentielle à sa survie. Celle-ci se vide petit à petit, et il doit aller la remplir au vaisseau. La quantité d'oxygène présente sur le vaisseau est limitée, mais il est possible d'en produire à partir du minerai qui se trouve sur la planète.

La survie de l'équipage est cependant compromise par la présence d'extraterrestres sur la planète. Ils pourront être hostiles et carnivores, auquel cas il faudra tenter d'éviter leur chemin, ou pacifiques et herbivores, auquel cas ils deviendront une menace indirecte puisqu'ils tenteront de manger les plantations de fruits.

Une fois le vaisseau réparé, les crocosmautes peuvent repartir, mais vont s'écraser de nouveau, sur une autre planète différente de la première. Le but de l'équipage est de survivre ainsi sur le plus de planètes possible.


## TODO
* [x] Planète
    * [x] Gérer le changement de planète (reset partiel des agents, etc)
    * [x] Générer les crocosmautes de façon intelligente (classe en fonction des stats + pourcentage d'individus de chaque métier)
    * [x] Placer les crocosmautes
    * [x] Placer la fusée
    * [x] Placer les minerais sans blocage ni superposition
    * [x] Placer l'eau sans blocage ni superposition
    * [x] Placer les alien
    * [x] Placer la tanière
* [x] Vaisseau
    * [x] Classe Vaisseau
    * [x] Gérer la réparation + le signal de retour au vaisseau
    * [x] Méthode `chargerFruits`
    * [x] Méthode `dechargerFruits`
    * [x] Méthode `dechargerOxygene`
* [ ] Crocosmautes
    * [x] Créer la structure des fichiers Java
    * [x] Méthode `manger`
    * [x] Méthode `mourir`
    * [ ] Méthode `attaquer`
    * [x] Méthode `avancerVers(x,y)`
    * [x] Méthode `rechargerOxygene`
    * [x] Méthode `retourVaisseau` (suite à l'activation du booléen `signal`)
    * [x] Mineurs
        * [x] Stratégie des Mineurs
        * [x] Méthode `miner`
        * [x] Méthode `deposerMinerai` (pour poser le minerai à la fusée)
    * [x] Éclaireurs
        * [x] Stratégie des Éclaireurs
        * [x] Méthode `poserBalise`
    * [x] Jardiniers
        * [x] Stratégie des Jardiniers
        * [x] Méthode `planter`
        * [x] Méthode `arroser`
        * [x] Méthode `recolter`
    * [ ] Gérer la perte de satiété et d'oxygène au fil des tours
* [ ] Alien
    * [x] Classes AlienHerbivore et AlienCarnivore
    * [ ] Stratégie des AlienHerbivores
    * [ ] Stratégie des AlienCarnivores
* [x] Classe Balise
    * [x] Types de balise -> `enum`
    * [x] Méthode `step` -> Effet "trace"
* [x] Classe Eau
* [x] Classe Minerai
* [ ] GUI
    * [x] Faire des jolies images pour les Portrayals
    * [x] Changer dynamiquement le Portrayal d'un fruit selon son état de croissance
    * [x] Une couleur par type de balise
    * [x] Système de tiling de l'eau
    * [ ] Faire un fond un peu stylé